import 'package:flutter/material.dart';
import 'package:krungsri_credit_card/class/credit_card_info.dart';
import 'package:krungsri_credit_card/services/credit_card_service.dart';
import 'package:krungsri_credit_card/utils/formatter.dart';
import 'package:krungsri_credit_card/widgets/credit_card_swiper.dart';
import 'package:krungsri_credit_card/widgets/tab_view/tab_account_info.dart';
import 'package:krungsri_credit_card/widgets/tab_view/tab_billed.dart';
import 'package:krungsri_credit_card/widgets/tab_view/tab_unbilled.dart';

const String citizenId = '1111111111111';

void main() {
  runApp(const KrungsriCreditCard());
}

class KrungsriCreditCard extends StatelessWidget {
  const KrungsriCreditCard({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Krungsri credit card',
      theme: ThemeData(
        useMaterial3: true,
        primaryColor: Colors.grey,
        dividerColor: Colors.grey,
      ),
      home: const Home(),
    );
  }
}

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  late Future<CreditCardInfo?> futureCreditCard = CreditCardService.addCreditCard(citizenId);
  late CreditCardInfo creditCardInfo;
  late String cardNumber;

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      initialIndex: 0,
      child: Scaffold(
        body: FutureBuilder(
          future: futureCreditCard,
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.done) {
              bool isNotEmpty = snapshot.data != null && snapshot.data!.cards.isNotEmpty;
              if (isNotEmpty) {
                return CreditCardPage(creditCardInfo: snapshot.data!);
              } else {
                return const CreditCardNotFound();
              }
            }
            return const Center(child: Text('loading...'));
          },
        ),
      ),
    );
  }
}

class CreditCardPage extends StatefulWidget {
  final CreditCardInfo creditCardInfo;

  const CreditCardPage({
    super.key,
    required this.creditCardInfo,
  });

  @override
  State<CreditCardPage> createState() => _CreditCardPageState();
}

class _CreditCardPageState extends State<CreditCardPage> {
  late String cardNumber;
  int swiperIndex = 0;

  @override
  void initState() {
    super.initState();
    setState(() {
      cardNumber = widget.creditCardInfo.cards[0].cardNumber;
    });
  }

  void onSwipeCardImage(int index) {
    setState(() {
      cardNumber = widget.creditCardInfo.cards[index].cardNumber;
      swiperIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisSize: MainAxisSize.max,
      children: [
        SizedBox(
          height: MediaQuery.of(context).size.height / 4,
          child: CreditCardSwiper(
            cards: widget.creditCardInfo.cards,
            onSwipeCreditCard: onSwipeCardImage,
          ),
        ),
        const Divider(
          height: 1,
        ),
        TabBar(
          labelPadding: EdgeInsets.zero,
          tabs: const [
            Tab(text: 'ACCOUNT INFO'),
            Tab(text: 'UNBILLED'),
            Tab(text: 'BILLED'),
          ],
          labelColor: Theme.of(context).primaryColor,
          indicatorColor: Theme.of(context).primaryColor,
          labelStyle: const TextStyle(fontWeight: FontWeight.w700),
          indicatorSize: TabBarIndicatorSize.tab,
        ),
        Expanded(
          child: TabBarView(
            children: [
              TabAccountInfo(cardInfo: widget.creditCardInfo.cards[swiperIndex]),
              TabUnbilled(cardNumber: cardNumber),
              TabBilled(cardNumber: cardNumber),
            ],
          ),
        ),
      ],
    );
  }
}

class CreditCardNotFound extends StatelessWidget {
  const CreditCardNotFound({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text('Citizen id: $citizenId', style: Formatter.h2),
          Text('Has no credit card.', style: Formatter.textStyleTitle),
        ],
      ),
    );
  }
}
