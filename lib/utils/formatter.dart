import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class Formatter {
  // Date fomat
  static String ddMMM = 'dd MMM';
  static String ddMMMyyyy = 'dd MMM yyyy';

  // Text style
  static TextStyle textStyleTitle = const TextStyle(fontSize: 18, fontWeight: FontWeight.w400);
  static TextStyle textStyleLabelTitle = const TextStyle(fontSize: 14, fontWeight: FontWeight.w300);
  static TextStyle h1 = const TextStyle(fontSize: 28, fontWeight: FontWeight.bold);
  static TextStyle h2 = const TextStyle(fontSize: 24, fontWeight: FontWeight.bold);
  static TextStyle h3 = const TextStyle(fontSize: 18, fontWeight: FontWeight.bold);

  static String formatDate(DateTime date, String format) {
    DateFormat formatter = DateFormat(format);
    return formatter.format(date);
  }

  static String numberWithDigit(double val) {
    return NumberFormat('#,###.##').format(val);
  }

  static String numberWithoutDigit(double val) {
    return NumberFormat('#,###').format(val);
  }
}
