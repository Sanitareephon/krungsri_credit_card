import 'dart:convert';

import 'package:krungsri_credit_card/class/credit_card_statement.dart';

CreditCardInfo creditCardInfoFromJson(String str) =>
    CreditCardInfo.fromJson(json.decode(str));

String creditCardInfoToJson(CreditCardInfo data) => json.encode(data.toJson());

class CreditCardInfo {
  String citizenId;
  List<UserCard> cards;

  CreditCardInfo({
    required this.citizenId,
    required this.cards,
  });

  factory CreditCardInfo.fromJson(Map<String, dynamic> json) => CreditCardInfo(
        citizenId: json["citizenId"],
        cards:
            List<UserCard>.from(json["cards"].map((x) => UserCard.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "citizenId": citizenId,
        "cards": List<dynamic>.from(cards.map((x) => x.toJson())),
      };
}

class UserCard {
  String cardName;
  String cardHolderName;
  String cardNumber;
  String cardImageUrl;
  double creditLimit;
  double availableCredit;
  double minPay;
  double fullPay;
  DateTime statementDate;
  DateTime dueDate;
  List<CreditCardStatement>? unBilled;

  UserCard({
    required this.cardName,
    required this.cardHolderName,
    required this.cardNumber,
    required this.cardImageUrl,
    required this.creditLimit,
    required this.availableCredit,
    required this.minPay,
    required this.fullPay,
    required this.statementDate,
    required this.dueDate,
    this.unBilled,
  });

  factory UserCard.fromJson(Map<String, dynamic> json) => UserCard(
        cardName: json["cardName"],
        cardHolderName: json["cardHolderName"],
        cardNumber: json["cardNumber"],
        cardImageUrl: json["cardImageUrl"],
        creditLimit: json["creditLimit"],
        availableCredit: json["availableCredit"],
        minPay: json["minPay"],
        fullPay: json["fullPay"],
        statementDate: DateTime.parse(json["statementDate"]),
        dueDate: DateTime.parse(json["dueDate"]),
      );

  Map<String, dynamic> toJson() => {
        "cardName": cardName,
        "cardHolderName": cardHolderName,
        "cardNumber": cardNumber,
        "cardImageUrl": cardImageUrl,
        "creditLimit": creditLimit,
        "availableCredit": availableCredit,
        "minPay": minPay,
        "fullPay": fullPay,
        "statementDate": statementDate.toIso8601String(),
        "dueDate": dueDate.toIso8601String(),
        "unBilled": creditCardStatementToJson(unBilled!)
      };
}
