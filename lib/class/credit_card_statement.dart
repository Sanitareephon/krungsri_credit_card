// To parse this JSON data, do
//
//     final creditCardStatement = creditCardStatementFromJson(jsonString);

import 'dart:convert';

List<CreditCardStatement> creditCardStatementFromJson(String str) =>
    List<CreditCardStatement>.from(
        json.decode(str).map((x) => CreditCardStatement.fromJson(x)));

String creditCardStatementToJson(List<CreditCardStatement> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class CreditCardStatement {
  String description;
  DateTime statementDate;
  double amount;

  CreditCardStatement({
    required this.description,
    required this.statementDate,
    required this.amount,
  });

  factory CreditCardStatement.fromJson(Map<String, dynamic> json) =>
      CreditCardStatement(
        description: json["description"],
        statementDate: DateTime.parse(json["statementDate"]),
        amount: json["amount"],
      );

  Map<String, dynamic> toJson() => {
        "description": description,
        "statementDate": statementDate.toIso8601String(),
        "amount": amount,
      };
}
