import 'package:http/http.dart' as http;
import 'package:http/http.dart';
import 'package:krungsri_credit_card/class/credit_card_info.dart';
import 'package:krungsri_credit_card/class/credit_card_statement.dart';

String baseUrl = "card-management-eajwtocuqa-as.a.run.app";

class CreditCardService {
  // Add new credit card
  static Future<CreditCardInfo?> addCreditCard(String citizenId) async {
    Uri url = Uri.https(baseUrl, "/v1/cards/$citizenId");
    Response response = await http.get(url);
    bool isOk = response.statusCode == 200;

    return isOk ? creditCardInfoFromJson(response.body) : null;
  }

  // Get billed
  static Future<List<CreditCardStatement>> getBilled(String cardNumber, String asOf) async {
    Map<String, String> params = {'cardNumber': cardNumber, 'asOf': asOf};
    Uri url = Uri.https(
      baseUrl,
      "/v1/billed-statements",
      params,
    );
    Response response = await http.get(url);
    bool isOk = response.statusCode == 200;

    return isOk ? creditCardStatementFromJson(response.body) : [];
  }

  // Get unbilled
  static Future<List<CreditCardStatement>> getUnBilled(String? cardNumber) async {
    Uri url = Uri.https(baseUrl, "/v1/unbilled-statements", {'cardNumber': cardNumber});
    Response response = await http.get(url);
    bool isOk = response.statusCode == 200;

    return isOk ? creditCardStatementFromJson(response.body) : [];
  }
}
