import 'package:flutter/material.dart';
import 'package:krungsri_credit_card/class/credit_card_info.dart';
import 'package:krungsri_credit_card/utils/formatter.dart';

class TabAccountInfo extends StatelessWidget {
  final UserCard cardInfo;

  const TabAccountInfo({super.key, required this.cardInfo});

  @override
  Widget build(BuildContext context) {
    return ListView(
      padding: const EdgeInsets.all(0),
      itemExtent: 35.0,
      children: <Widget>[
        // Credit
        ListTile(
          leading: Text('AVAILABLE CREDIT', style: Formatter.textStyleLabelTitle),
          trailing: Text('CREDIT LIMIT', style: Formatter.textStyleLabelTitle),
        ),
        ListTile(
          leading: Text(
            Formatter.numberWithDigit(cardInfo.availableCredit),
            style: Formatter.h1,
          ),
          trailing: Text(
            Formatter.numberWithoutDigit(cardInfo.creditLimit),
            style: Formatter.h1,
          ),
        ),
        const SizedBox(),
        const SizedBox(),
        const SizedBox(),

        // Pay amount
        ListTile(
          leading: Text(
            'MIN PAY',
            style: Formatter.textStyleLabelTitle,
          ),
          trailing: Text(
            'FULL PAY',
            style: Formatter.textStyleLabelTitle,
          ),
        ),
        ListTile(
          leading: Text(
            Formatter.numberWithDigit(cardInfo.minPay),
            style: Formatter.h2,
          ),
          trailing: Text(
            Formatter.numberWithDigit(cardInfo.fullPay),
            style: Formatter.h2,
          ),
        ),
        const Divider(),

        // Date
        ListTile(
          leading: Text('STATEMENT DATE', style: Formatter.textStyleLabelTitle),
          trailing: Text('DUE DATE', style: Formatter.textStyleLabelTitle),
        ),
        ListTile(
          leading: Text(
            Formatter.formatDate(cardInfo.statementDate, Formatter.ddMMMyyyy),
            style: Formatter.h2,
          ),
          trailing: Text(
            Formatter.formatDate(cardInfo.dueDate, Formatter.ddMMMyyyy),
            style: Formatter.h2,
          ),
        ),
      ],
    );
  }
}
