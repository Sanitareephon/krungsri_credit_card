import 'package:flutter/material.dart';
import 'package:krungsri_credit_card/services/credit_card_service.dart';
import 'package:krungsri_credit_card/widgets/tab_view/statement_detail.dart';

class TabUnbilled extends StatefulWidget {
  final String cardNumber;

  const TabUnbilled({
    super.key,
    required this.cardNumber,
  });

  @override
  State<TabUnbilled> createState() => _TabUnbilledState();
}

class _TabUnbilledState extends State<TabUnbilled> {
  @override
  Widget build(BuildContext context) {
    List<StatementDetail> statementWidget = List.empty(growable: true);

    return FutureBuilder(
      future: CreditCardService.getUnBilled(widget.cardNumber),
      initialData: const [],
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          statementWidget = snapshot.data!
              .map((item) => StatementDetail(
                    title: item.description,
                    amount: item.amount,
                    dateTime: item.statementDate,
                  ))
              .toList();

          return ListView.separated(
            padding: const EdgeInsets.all(0),
            itemCount: statementWidget.length,
            itemBuilder: (context, index) => statementWidget[index],
            separatorBuilder: (context, index) => const Divider(),
          );
        }

        return Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.end,
          children: [LinearProgressIndicator(color: Theme.of(context).primaryColor)],
        );
      },
    );
  }
}
