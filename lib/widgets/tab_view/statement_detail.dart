import 'package:flutter/material.dart';
import 'package:krungsri_credit_card/utils/formatter.dart';

class StatementDetail extends StatelessWidget {
  final String title;
  final double amount;
  final DateTime dateTime;

  const StatementDetail({
    super.key,
    required this.title,
    required this.amount,
    required this.dateTime,
  });

  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            title,
            style: Formatter.textStyleTitle,
          ),
          Text('${Formatter.numberWithoutDigit(amount)} THB', style: Formatter.textStyleTitle),
        ],
      ),
      subtitle: Text(
        Formatter.formatDate(dateTime, Formatter.ddMMM),
        style: const TextStyle(fontWeight: FontWeight.w500),
      ),
    );
  }
}
