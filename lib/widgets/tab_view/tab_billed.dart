import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:krungsri_credit_card/services/credit_card_service.dart';
import 'package:krungsri_credit_card/utils/formatter.dart';
import 'package:krungsri_credit_card/widgets/tab_view/statement_detail.dart';

List<String> monthList = <String>['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

List<String> dropdownDisplayMonth() {
  List<String> displayMonth = List.empty(growable: true);
  DateTime currentDate = DateTime.now();

  for (int i = 0; i < currentDate.month; i++) {
    displayMonth.add('${monthList[i]} ${currentDate.year}');
  }

  return displayMonth;
}

List<String> dropdownList = dropdownDisplayMonth();

class TabBilled extends StatefulWidget {
  final String cardNumber;

  const TabBilled({super.key, required this.cardNumber});

  @override
  State<TabBilled> createState() => _TabBilledState();
}

class _TabBilledState extends State<TabBilled> {
  String currentAsOf = dropdownList.last;

  String formatAsOfDate(currentAsOf) {
    DateFormat dateFormat = DateFormat('MMM yyyy dd');
    DateFormat dateAsOfFormat = DateFormat('MMyyyy');
    DateTime selectDate = dateFormat.parse('$currentAsOf 01');
    String formattedAsOfDate = dateAsOfFormat.format(selectDate);
    return formattedAsOfDate;
  }

  void onChangedBilledMonth(String asOf) {
    setState(() {
      currentAsOf = asOf;
    });
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> statementWidget = List.empty(growable: true);

    Widget asOfWidget = ListTile(
      title: Text(
        'STATEMENT OF',
        style: Formatter.h3,
      ),
      trailing: DropdownStatementAsOf(
        changeBilledMonth: onChangedBilledMonth,
        asOf: currentAsOf,
      ),
    );

    return FutureBuilder(
      future: CreditCardService.getBilled(widget.cardNumber, formatAsOfDate(currentAsOf)),
      initialData: const [],
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          statementWidget.add(asOfWidget);

          for (int i = 0; i < snapshot.data!.length; i++) {
            statementWidget.add(
              StatementDetail(
                title: snapshot.data![i].description,
                amount: snapshot.data![i].amount,
                dateTime: snapshot.data![i].statementDate,
              ),
            );
          }

          return ListView.separated(
            padding: const EdgeInsets.all(0),
            itemCount: statementWidget.length,
            itemBuilder: (context, index) => statementWidget[index],
            separatorBuilder: (context, index) {
              return index == 0
                  ? const DashedLine(
                      color: Colors.grey,
                    )
                  : const Divider();
            },
          );
        }

        return Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [asOfWidget, LinearProgressIndicator(color: Theme.of(context).primaryColor)],
        );
      },
    );
  }
}

class DropdownStatementAsOf extends StatefulWidget {
  final void Function(String asOf) changeBilledMonth;
  final String asOf;

  const DropdownStatementAsOf({
    super.key,
    required this.changeBilledMonth,
    required this.asOf,
  });

  @override
  State<DropdownStatementAsOf> createState() => _DropdownStatementAsOfState();
}

class _DropdownStatementAsOfState extends State<DropdownStatementAsOf> {
  onChangeBilledMonth(String? value) {
    widget.changeBilledMonth(value!);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          border: Border.all(
            color: Theme.of(context).primaryColor,
            style: BorderStyle.solid,
          ),
          borderRadius: BorderRadius.circular(3)),
      child: DropdownButtonHideUnderline(
        child: DropdownButton<String>(
          value: widget.asOf,
          icon: const Icon(Icons.arrow_drop_down_rounded),
          elevation: 16,
          padding: const EdgeInsets.all((8)),
          isDense: true,
          borderRadius: BorderRadius.circular(3),
          onChanged: onChangeBilledMonth,
          items: dropdownList.map<DropdownMenuItem<String>>(
            (String value) {
              return DropdownMenuItem<String>(
                value: value,
                child: Text(value),
              );
            },
          ).toList(),
        ),
      ),
    );
  }
}

class DashedLine extends StatelessWidget {
  const DashedLine({Key? key, this.height = 1, this.color = Colors.grey}) : super(key: key);
  final double height;
  final Color color;

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (BuildContext context, BoxConstraints constraints) {
        final boxWidth = constraints.constrainWidth();
        const dashWidth = 10.0;
        final dashHeight = height;
        final dashCount = (boxWidth / (2 * dashWidth)).floor();
        return Flex(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          direction: Axis.horizontal,
          children: List.generate(dashCount, (_) {
            return SizedBox(
              width: dashWidth,
              height: dashHeight,
              child: DecoratedBox(
                decoration: BoxDecoration(color: color),
              ),
            );
          }),
        );
      },
    );
  }
}
