import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:krungsri_credit_card/class/credit_card_info.dart';

class CreditCardSwiper extends StatefulWidget {
  final Function(int index) onSwipeCreditCard;
  final List<UserCard> cards;

  const CreditCardSwiper({
    super.key,
    required this.onSwipeCreditCard,
    required this.cards,
  });

  @override
  State<CreditCardSwiper> createState() => _CreditCardSwiperState();
}

class _CreditCardSwiperState extends State<CreditCardSwiper> {
  @override
  Widget build(BuildContext context) {
    List<Widget> imageSliders = widget.cards
        .map((card) => ClipRRect(
            borderRadius: const BorderRadius.all(Radius.circular(5.0)),
            child: Stack(
              children: <Widget>[
                Image.network(card.cardImageUrl),
              ],
            )))
        .toList();

    return CarouselSlider(
      options: CarouselOptions(
        padEnds: true,
        aspectRatio: 2,
        disableCenter: true,
        animateToClosest: true,
        enlargeFactor: 1,
        enlargeCenterPage: true,
        enableInfiniteScroll: false,
        onPageChanged: (index, reason) => widget.onSwipeCreditCard(index),
      ),
      items: imageSliders,
    );
  }
}
