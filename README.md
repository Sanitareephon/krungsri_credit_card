# Krungsri Credit Card Assignment
Krungsri Credit Card Assignment is an assignment for testing purpose.
To create mobile application with flutter to display credit card information for a Krungsri credit card user.


## Getting started

These instructions will help you get a copy of the project up on your local machine.

### Installation

1. Clone the repository to your local machine.
2. Navigate to the project directory.
3. Run `flutter pub run get` to install all project dependencies. 

### Running the Project

To run the project in development mode, execute the following command:

```At main.dart file.
Press F5 to start proect.
```

## Built With

This project relies on several key technologies:

- [Flutter](https://flutter.dev/) - Build apps for any screen, multi-platform applications from a single codebase..
- [Dart](https://dart.dev/) - Dart is a client-optimized language for fast apps on any platform.
- [intl](https://pub.dev/documentation/intl/latest/intl/intl-library.html) - Provides internationalization and localization facilities.
- [carousel_slider](https://pub.dev/packages/carousel_slider) - Simply create a CarouselSlider widget.
- [http](https://pub.dev/packages/http) -  composable, Future-based library for making HTTP requests.
- [flutter_launcher_icons](https://pub.dev/packages/flutter_launcher_icons) - A command-line tool which simplifies the task of updating your Flutter app's launcher icon.
